//
//  Recipe.swift
//  07- MisRecetas
//
//  Created by Pablo  on 18/12/2019.
//  Copyright © 2019 Pablo Benzo. All rights reserved.
//

import Foundation
import UIKit

class Recipe : NSObject {
    let name : String
    let image : UIImage
//    var time : String
    let ingredients : [String]
    let steps : [String]
    let info : String

    var rating : String = "agregar"
    
    init(name: String, ingredients: [String], steps: [String], image: UIImage, info: String) {
        self.name = name
        self.image = image
        self.ingredients = ingredients
        self.steps = steps
        self.info = info
    }
    
    /*
     
    init?(time: String) {
        self.time = time
        if time.isEmpty {
            return nil
        }
    }
 
    */
    
}



