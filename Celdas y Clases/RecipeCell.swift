//
//  RecipeCell.swift
//  07- MisRecetas
//
//  Created by Pablo  on 20/12/2019.
//  Copyright © 2019 Pablo Benzo. All rights reserved.
//

import UIKit

class RecipeCell: UITableViewCell {

    @IBOutlet var miniImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var ingredientsLabel: UILabel?
    

}
