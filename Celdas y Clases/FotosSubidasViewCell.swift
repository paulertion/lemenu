//
//  FotosSubidasViewCell.swift
//  07- MisRecetas
//
//  Created by Pablo  on 05/03/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit

class FotosSubidasViewCell: UICollectionViewCell {
    
    @IBOutlet var celdaImagen: UIImageView!    
}
