//
//  AppDelegate.swift
//  07- MisRecetas
//
//  Created by Pablo  on 15/12/2019.
//  Copyright © 2019 Pablo Benzo. All rights reserved.
//

import UIKit
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?   // Parse

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        sleep(1)
        
        UINavigationBar.appearance().barTintColor = UIColor(displayP3Red: 0.7, green: 0.4, blue: 0.4, alpha: 1.0) //color barra navegacion
        UINavigationBar.appearance().tintColor = UIColor.white  // color texto boton de regreso
        if let barFont = UIFont(name: "Copperplate", size: 22.0){   // tamaño y fuente barra navegacion
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font: barFont]
        }
        
        // Parse IN +++
        
        Parse.enableLocalDatastore()
        
        let parseConfiguration = ParseClientConfiguration(block: { (ParseMutableClientConfiguration) -> Void in
            ParseMutableClientConfiguration.applicationId = "myappID"
            ParseMutableClientConfiguration.clientKey = "qK4YXiyq47Bo"
            ParseMutableClientConfiguration.server = "http://ec2-3-22-186-169.us-east-2.compute.amazonaws.com/parse"
        })
        
        Parse.initialize(with: parseConfiguration)
     //   testParseSave()
        return true
    }
    
    func testParseSave() {
        let testObject = PFObject(className: "MyTestObject")
        testObject["Recetas"] = "Platillo"
        testObject.saveInBackground { (success, error) -> Void in
            if success {
                print("El objeto se ha guardado en Parse correctamente.")
            } else {
                if error != nil {
                    print("No es NULO:")
                    print (error as Any)
                } else {
                    print("SI es NULO:")
                    print ("Error")
                }
            }
        }
    }

    // Parse OUT +++
    
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

//
//
//    //--------------------------------------
//    // MARK: Push Notifications
//    //--------------------------------------
//
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let installation = PFInstallation.current()
//        installation!.setDeviceTokenFrom(deviceToken)
//        installation!.saveInBackground()
//
//        /*let appName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
//
//
//        PFPush.subscribeToChannel(inBackground: "") { (succeeded: Bool, error: NSError?) in
//            if succeeded {
//                print("\(appName) se ha registrado correctamente para recibir push notifications.\n");
//            } else {
//                print("\(appName) no ha podido registrarse para recibir push notifications debido al error = %@.\n", error)
//            }
//        }*/
//    }
//
//    private func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
//        if error.code == 3010 {
//            print("Las Push notifications no funcionan en el Simulador de iOS.\n")
//        } else {
//            print("application:didFailToRegisterForRemoteNotificationsWithError: %@\n", error)
//        }
//    }
//
//    private func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
//        PFPush.handle(userInfo)
//        if application.applicationState == UIApplication.State.inactive {
//            PFAnalytics.trackAppOpened(withRemoteNotificationPayload: userInfo)
//        }
//    }
//
//    ///////////////////////////////////////////////////////////
//    // Uncomment this method if you want to use Push Notifications with Background App Refresh
//    ///////////////////////////////////////////////////////////
//    // func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
//    //     if application.applicationState == UIApplicationState.Inactive {
//    //         PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(userInfo)
//    //     }
//    // }
//
//
