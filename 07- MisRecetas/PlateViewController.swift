//
//  PlateViewController.swift
//  07- MisRecetas
//
//  Created by Pablo  on 16/01/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit

class PlateViewController: UITableViewController {
    
    var comida : Recipe!        // traigo la receta elegida del UIVC anterior
    var platos : [Recipe] = []
    var menu : [Recipe] = []    // traigo RECETAS del UIVC anterior
         

    override func viewDidLoad() {
        super.viewDidLoad()

        // Configurar texto del boton BACK
               navigationItem.backBarButtonItem = UIBarButtonItem(title: "Volver", style: .plain, target: nil, action: nil)
        
        tableView.separatorColor = UIColor(displayP3Red: 0.5, green: 0.5, blue: 0.5, alpha: 0.4)
        // Alpha separador recetas
                
            // ENSALADAS
                       
        var plate = Recipe(name: "CESAR", ingredients: ["> Mix de lechugas (capuccina o criolla)","> 1 pechuga de pollo","> 2 rodajas de pan","> 40 gramos de queso parmesano","> Salsa Cesar: 3 filetes de anchoas, un huevo, jugo de medio limón, 250cc de aceite, 1 diente de ajo, 1 cucharada mostaza, 1 cucharada de miel, 50grs de parmesano rayado, sal y pimienta"], steps: ["1. Cortar la pechuga de pollo, cortar en tiras y reservar.", "2. Tostar el pan, cortar en cuadrados y reservar.","3. Lavar, secar y cortar la lechuga. Disponer en un bowl.","4. Agregar el pollo, los croutons previamente reservados, queso parmesano rayado en láminas.","5. Agregar salsa Cesar.","Para la salsa:","1. Mixear todos los ingredientes menos el aceite.","2. Cuando la mezcla este homogenea incorporar el aceite en forma de hilo (emulcionar)."], image: #imageLiteral(resourceName: "ENSALADA Cesar"), info: "Para un almuerzo saludable")
               platos.append(plate)
               
        plate = Recipe(name: "TABULÉ", ingredients: ["> 1/2 taza de couscous (trigo burgol)","> 2 tomates","> 1 cebolla chica","> 1 taza de perejil","> 1/4 taza de menta","> Sal, 1 limon y aceite de oliva"], steps: ["1. Remojar el couscous. Cubrir apenas con agua hirviendo y dejar reposar media hora hasta que absorva toda el agua. Reservar.", "2. Cortar en brunoisse el tomate y la cebolla.", "3. Picar la menta y el perejil.","4. Separar el couscous con un tenedor y agregar el resto de los ingredientes. Mezclar.","5. Condimentar con aceite de oliva, sal y jugo de un limón."], image: #imageLiteral(resourceName: "ENSALADA Tabule"), info: "Una opción fresca")
               platos.append(plate)
               
        plate = Recipe(name: "CAPRESE CON PASTA SECA", ingredients: ["> 200 gramos de pasta seca","> 150grs de muzzarella o queso fresco","> 1 taza de tomates cherry","> 1 puñado de albahaca fresca","> Aceite de oliva","> Sal y pimienta c/n"], steps: ["1. Cocinar la pasta seca como indica el paquete. Enfriar.","2. Cortar los tomates al medio, la muzzarela cortarla en cubos y cortar groseramente la albahaca.","3. Mezclar todos los ingredientes y condimentar."], image: #imageLiteral(resourceName: "ENSALADA Caprese"), info: "Un clásico que no falla")
               platos.append(plate)
               
        plate = Recipe(name: "COLESLAW", ingredients: ["> 1/2 repollo blanco","> 2 zanahorias","> 20cc de vinagre de manzana","> 60grs de crema de leche","> 60grs de mayonesa","> 20grs de azucar","> Sal y pimienta a gusto"], steps: ["1. Cortar el repollo fino o en mandolina.","2. Rayar la zanahoria lo más fino posible.","3. Para el aderezo: mezclar la mayonesa, el vinagre, la crema de leche junto con pimienta, azucar y sal.","4. Mezclar todos los ingredientes en un bowl."], image: #imageLiteral(resourceName: "ENSALADA Coleslaw"), info: "Simple y deliciosa")
               platos.append(plate)
               
        plate = Recipe(name: "MIMOSA", ingredients: ["> 2 papas","> 2 zanahorias","> 3 huevos","> 200grs de Mayonesa","> 1 lata de atun","> Sal y pimienta c/n"], steps: ["1. Cocinar los huevos por 10 minutos. Cortar cocción con agua fria.", "2. Al mismo tiempo hervimos en otra olla las papas y las zanahorias hasta que esten tiernas. Dejar enfriar.","3. Una vez que los huevos estan frios separar claras de yemas.","4. Con la parte gruesa del rayador rayamos las papas, las claras y las zanahorias. Reservamos.","5. Sobre una fuente de 20x15 aproximadamente, disponemos una capa de papa rayada. Seguidamente sobre la papa ponemos una capa de mayonesa.","6. Luego colocamos el atun y sobre el mismo extendemos una capa ligera de mayonesa.","7. Sobre esta capa colocamos la clara de huevo, y sobre el huevo extendemos una fina capa de mayonesa.","8. Seguidamente ponemos una capa de zanahoria rayada y extendemos una última capa de mayonesa.","9. Por último, ponemos una capa de la yema desmenuzada. Reposar en la heladera."], image: #imageLiteral(resourceName: "ENSALADA Mimosa"), info: "Made in Ucrania")
               platos.append(plate)
        
            // PLATOS
        
        plate = Recipe(name: "COLITA DE CUADRIL CON VERDURAS", ingredients: ["> 1 unidad de colita de cuadril (1 1⁄2 kg)","> 6 cucharadas soperas de aceite","> 1 cabeza de ajo","> Pimentón, sal, pimienta, comino (c/n)","> 4 papas y 4 batatas","> 2 cebollas moradas","1 aji rojo y 1 aji verde","1 cucharada sopera de almidón de maiz"], steps: ["1. Colocar la colita de cuadril en una asadera para horno previamente untada con aceite y los condimentos junto con el ajo y los morrones. Masajear.", "2. Agregar las papas o batatas y las cebollas cortadas en trozos.","3. Agregar un chorrito de agua a la fuente.","4. Cocinar en horno fuerte 15 mins.","5. Rociar toda la preparación con el liquido formado en la fuente.","6. Continuar la cocción a fuego moderado hasta que la carne este tierna y las papas o batatas doradas.","7. Al jugo de cocción de la asadera agregarle 2 cucharadas de agua caliente y espesar con el almidon de maiz disuelto en un poquito de agua.","8. Salsear la carne y servir."], image: #imageLiteral(resourceName: "CLASICA Carne"), info: "Para amantes de la carne")
            platos.append(plate)
        
        plate = Recipe(name: "ALITAS DE POLLO A LA BBQ", ingredients: ["> 8 alitas de pollo","> 8 cucharadas abundantes de alsa barbacoa","> 1 Cebolla","> 2 cucharadas de aceite","> 1 diente de ajo","> 1 litro de caldo de verdura"], steps: ["1. Picar el ajo y cortar la cebolla en juliana.", "2. En una sarten calentar el aceite y sellar ambos lados de las alitas (bien doradas).", "3. Agregar la cebolla y rehogar.","4. Una vez que las cebollas esten doradas agregar el caldo mas 4 cucharadas de salsa barbacoa y cocinar a fuego bajo hasta que esten bien cocidas.","5. Esperar a que se reduzca un poco el liquido y agregar el resto de la barbacoa."], image: #imageLiteral(resourceName: "CLASICA Pollo"), info: "Alitas de rechupete")
        platos.append(plate)
        
        plate = Recipe(name: "PAPILLOTE DE SALMON", ingredients: ["> 1 filet de salmón cortado en porciones de 100 grs","> 1 limón","> Aceite de oliva c/n","> sal y pimienta c/n","> Aromaticas: romero, tomillo, o eneldo","> papel manteca o aluminio"], steps: ["1. Sobre rectangulos de papel manteca o aluminio poner un chorrito de aceite de oliva, un colchón de rodajas de limón.", "2. Colocamos por encima la pieza de salmón y por encima de ésta condimentamos con sal, pimienta, una ramita de aromatica y rodajas de limón.", "3. Cerramos el papel a modo de repulgue de empanada (totalmente cerrado).","4. Llevamos a horno precalentado a 180º por 20 minutos aproximadamente.","5. Servir directamente en el papillote.","Opcional: poner un colchon de esparragos o las verduras que mas te gusten."], image: #imageLiteral(resourceName: "CLASICA Salmon"), info: "Alternativa pescetariana")
        platos.append(plate)
        
        plate = Recipe(name: "QUICHE LORRAINE", ingredients: ["La masa:","> 200 grs de harina","> 100 grs de manteca, un huevo","> Sal c/n,","El relleno:","> 4 huevos","> 1/2 taza de crema de leche","> 1/2 taza de leche","> 100 grs de panceta","> Queso en hebras a gusto","> Sal y pimienta c/n"], steps: ["Para la masa:", "1. Mezclar la harina con la manteca hasta formar un arenado y luego unir con el resto de los ingredientes.","2. Dejar descansar la masa envuelta en papel film aproximadamente 30 minutos en la heladera.","3. Estirar, forrar el molde y reservar en la heladera.","Para el relleno:","1. Cortar la panceta en lardons.","2. Saltear hasta dorar y reservar.","3. Mezclar los huevos con la crema, la leche, la sal y pimienta.","4. Llevar la masa al horno a 180º por 5 minutos sin relleno (con peso).","5. Agregar la panceta y el queso en hebras a la mezcla de huevos.","6. Verter la mezcla sobre la masa precocida y llevar a horno por 15 minutos mas hasta que la mezcla coagule y este dorada."], image: #imageLiteral(resourceName: "CLASICA Tarta"), info: "Tarta con panceta estilo Frances")
        platos.append(plate)
        
        plate = Recipe(name: "PIZZA MARGARITA", ingredients: ["> 120cc de agua","> 5 grs de albahaca fresca","> 200 grs de harina 0000","> 5 grs de sal","> 5 grs de levadura","> 150 grs de tomates frescos","> 30 grs de aceitunas verdes","> 200 grs de muzzarella"], steps: ["1. Colocar la harina en un bowl, agregar levadura y mezcle, luego incorporar agua y sal, mezclar hasta formar la masa.", "2. Retirar la masa del bowl y amasarla sobre la mesada enharinada hasta formar un bollo. Envolver el bollo en papel film y dejar levar durante 1 hora, una vez transcurrido el tiempo de reposo amasar nuevamente con las manos dando forma circular, luego colocar sobre una placa y dejar levar nuevamente. Cocinar en horno precalentado a 180°C durante 10 minutos.", "3. Cortar el diente de ajo en brunoise.","4. Cortar la albahaca en chiffonade.","5. Una vez lista, cortar la masa con mozzarella, luego rodajas de tomate y ajo picado, terminar la cocción en horno a 180°C durante 15 minutos.","6. Al momento de servir incorporar la albahaca y las aceitunas.","7. Cortar en porciones a gusto."], image: #imageLiteral(resourceName: "CLASICA Pizza"), info: "La pizza más rica")
        platos.append(plate)
        
            // VEGETARIANAS
        
        plate = Recipe(name: "ZUCCHINIS RELLENOS DE QUINOA", ingredients: ["> 1 cucharadita de ajo en escamas","> 3 zapallitos alargados","> 1 cebolla","> 2 cucharadas de pan rallado","> 1/2 taza de queso crema","> 1 taza de quinoa cocida","> 1 taza de queso rallado"], steps: ["1. Cortar los zapallitos a lo largo, con una cuchara ahuecarlos con cuidado y reservar la pulpa.", "2. En una sartén con aceite saltear la cebolla ya picada con los morrones.","3. Salpimentar y agregar sobre el final, la pulpa de los zapallitos picada.","4. En un recipiente, mezclar la preparación anterior con la quinoa, el queso crema y la mitad del queso rallado.","5. Disponer los zapallitos en una fuente para horno enmantecada y rellenar.","6. Terminar con una mezcla de queso rallado, pan rallado y ajo en escamas.","7. Llevar a un horno fuerte hasta que se cocinen y gratinen."], image: #imageLiteral(resourceName: "VEG Quinoa"), info: "Una receta con personalidad")
        platos.append(plate)
        
        plate = Recipe(name: "PUCHERO CON CUBOS DE SEITAN", ingredients: ["Para la salsa pesto:","> 3 cucharadas de queso parmesano rayada","> 1/2 taza de perejil picado","> 2 dientes de ajo","> Sal y pimienta c/n","> 50 grs de nueces picadas","Para el puchero:","> 1 choclo","> 1 batata","> 2 zanahorias","> 3 ramitas de romero","> 2 cebollas coloradas","> 2 zapallitos redondos","> 1 cabeza de ajo","> Sal de mar a gusto","> 1/2 calabaza","> 4 ramas de apio","> 2 tallos de puerro","> 1 papa","> 3 ramas de tomillo","> 1 ramillete de perejil","> 2 cucharadas de aceite de oliva","> 1 cucharadita de pimienta en grano","> 1 taza de porotos","Para el Seitan:","> 2 hojas de laurel","> Agua c/n","> 2kg de harina de alto porcentaje de gluten","> 1 cabeza de ajo","> 50 grs de alga kombu","> 1 nuez de jengibre","> 1 cucharada de pimienta blanca en grano","> 1 taza de salsa de soja"], steps: ["Para el Seitan:", "1. Colocar en un bowl la harina y agregar agua de a poco mientras amasa hasta conseguir que la masa se despegue del bowl.","2. Luego amasar sobre la mesada durante aproximadamente 15 minutos, hasta conseguir una masa bien lisa. Terminado este paso coloque nuevamente la masa en el bowl y cubrirla con agua fría, envolver con papel film y dejar reposar durante 30 minutos.","3. Transcurrido el tiempo de reposo quitar el agua y lavar la masa con agua fría las veces que sea necesario hasta conseguir que el agua quede transparente.","4. Preparar un caldo, colocar en una cacerola abundante agua fría, los granos de pimienta, salsa de soja, alga kombu, las hojas de laurel, la cabeza de ajo con piel cortada al medio, jengibre y por ultimo incorporar el seitan, cocinear a fuego mínimo durante aproximadamente 1 hora. El seitan debe estar cubierto con líquido durante la cocción. Terminada la cocción dejar enfriar el seitan dentro del caldo.","Para el puchero:","1. Cortar las verduras en trozos del mismo tamaño y cortar la cabeza de ajo al medio","Para el pesto:","1. Mezclar todos los ingredientes en un bowl, condimentar con sal y pimienta y agregar el aceite de oliva en forma de hilo mientras mezclamos.","El plato en sí:","1. Una vez frió el seitan cortarlo en cubos.","2. En una cacerola profunda con aceite de oliva incorporar las verduras del puchero, los porotos, sal de mar, los granos de pimienta, el romero, tomillo, perejil y por último los cubos de seitan, cubrir con agua, tapar la cacerola y cocinar a fuego mínimo durante aproximadamente 1 hora.","3. Servir en un plato hondo una porción de puchero junto con cubos de seitan. Decorar a gusto con el pesto y si se quiere, con crutones de pan."], image: #imageLiteral(resourceName: "VEG Seitan"), info: "Carne sin carne")
        platos.append(plate)
        
        plate = Recipe(name: "ARROZ CON VERDURAS", ingredients: ["> Aceitunas negras c/n","> 3 cucharadas de aceite de oliva","> Alcaparras c/n","> 250 grs de arroz salvaje","> 1/2 cebolla roja","> 1 puñado de cebolla de verdeo fresca","> Cilantro fresco c/n","> 3 huevos cocidos","> 1 pimiento verde","> 100 grs de queso feta"], steps: ["1. Lavar bien el arroz y ponerlo a cocinar en agua hirviendo.", "2. Escurrir y dejar enfriar.", "3. Picar en cuadraditos pequeños el pimiento y la cebolla. Picar las aceitunas en ruedas finas. Picar finamente el cilantro y la cebolla de verdeo.","4. Cuando el arroz esté listo, poner en un bol y añadir las hierbas, el pimiento, la cebolla y las aceitunas ya cortadas.","5. Mezclar bien y a añadir las alcaparras, el aceite de oliva y el queso feta desmigado.","6. Agregar sal a gusto y decorar con unas hojas de cilantro y unos huevos cocidos cortados a la mitad."], image: #imageLiteral(resourceName: "VEG Arroz"), info: "Un arroz bien acompañado")
        platos.append(plate)
        
        plate = Recipe(name: "BROCHETAS VEGGIE", ingredients: ["> 1 pimiento rojo","> 1 pimiento verde","> 1 pimiento amarillo","> 10 Tomates cherry","> 1 Cebolla","> 1 zapallito","> 5 Champiñones","> Sal y aceite c/n","> Romero u orégano a gusto"], steps: ["1. Lavamos y cortamos todas las verduras en trozos más o menos del mismo tamaño para que se cocinen por igual.", "2. Untamos las verduras en aceite e hierbas aromáticas, las dejamos reposar 15 minutos para que se impregne bien el aroma.", "3. Pinchamos todas las verduras combinando los colores.","4. Esperamos a que estén doraditas, vuelta y vuelta y sacamos al plato.","5. Untamos con nuestras salsas favoritas y servir."], image: #imageLiteral(resourceName: "vegetariana"), info: "Otra forma de comer brochetas")
        platos.append(plate)
        
        plate = Recipe(name: "TORTILLA DE ZAPALLITO", ingredients: ["> 5 huevos","> 1 cebolla","> 1 zapallito grande o 2 pequeños","> Aceite de oliva y sal a gusto"], steps: ["1. Lava el zapallito y córtalo en rodajas finas.", "2. Poner las rodajas del zapallito en una sartén y cocínalo 8 minutos aproximadamente.", "3. Mientras se cocina el zapallito, pelar la cebolla y córtala en trocitos.","4. En una sartén colocar un par de cucharadas de aceite y a fuego medio-bajo, echar la cebolla junto con un poco de sal y dejar que se cocinen durante unos 10 minutos, hasta que empiece a dorarse.","5. Cuando la cebolla ya esté prácticamente lista incorporar el zapallito ya cocido y revolverlo todo unos 2 minutos a fuego medio. Servir."], image: #imageLiteral(resourceName: "VEG Tortilla"), info: "Menos popular pero muy rica")
        platos.append(plate)
       
            // GUARNICIONES
        
        plate = Recipe(name: "PAPAS RÚSTICAS", ingredients: ["> 3 a 4 papas","> Sal y pimienta a gusto","> 3 cucharadas de aceite de maiz","> Condimentos: orégano, provenzal, pimentón, pimienta, ajo molido"], steps: ["1. Lavar y cepillar las papas y cortar en jagos. Enjuagar y secar las papas.", "2. Llevar a freír hasta dorar.", "3. Mientras tanto en un bowl colocamos los condimentos con dos o tres cucharadas de aceite. Mezclar y reservar.","4. Una vez echas las papas ponerlas en asadera, agregar sal y a gusto y mezclar condimentos. Revolver con la ayuda de una espátula.","5. Llevar al horno para matenerlo caliente y se terminen de dorar.","Quedan muy ricas acompañadas con salsa de mayonesa y/o salsa de soja."], image: #imageLiteral(resourceName: "GUAR Papas"), info: "Clásicas con una cervecita")
        platos.append(plate)
        
        plate = Recipe(name: "PROVOLETA", ingredients: ["> 200gr de provoleta","> 1 cucharda de aceite de oliva","> 1 cucharadita de orégano"], steps: ["1. Cortar la provoleta en una rodaja de 200 gramos.", "2. Luego ubicarla en una provoletera de hierro con la parte dorada hacia arriba.", "3. Dejar unos minutos hasta que funda, sin que quede desarmada o derretida.","4. Servir directamente en la provoletera sobre una base de madera.","5. Rociar con orégano y terminar con aceite de oliva."], image: #imageLiteral(resourceName: "GUAR Provoleta"), info: "El queso que gusta a todos")
        platos.append(plate)
        
        plate = Recipe(name: "BASTONES DE MUZZARELLA", ingredients: ["> 500 gr. de queso muzzarella","> 150 gr. de harina","> 3 huevos batidos","> 300 gr. de pan rallado","> 1 cucharadita de oregano","> Sal a gusto","> 1 salsa a gusto"], steps: ["1. Se corta el queso muzzarella en bastones de 3cms", "2. Se baten los huevos y se condimentan con sal y orégano.", "3. Se pasa cada bastón por  harina, luego por el huevo y finalmente por el pan rallado.","4. Se reservan en la heladera durante 15 minutos para que se enfríe bien el queso (y no corramos el riesgo de que se chorree al freírlos).","5. Se fríen en abundante aceite y bien caliente hasta que queden doraditos y crujientes.","6. Se escurren en papel absorbente, se sirven con la salsa a gusto."], image: #imageLiteral(resourceName: "GUAR Bastones"), info: "Siempre sabrosos con salsa")
        platos.append(plate)
        
        plate = Recipe(name: "AROS DE CEBOLLA", ingredients: ["> 1 cebolla grande","> 1 vaso de harina de trigo","> 1 vaso de pan rallado","> 1 vaso de leche","> 1 huevo","> 1 cucharadita de levadura química en polvo","> 2 vasos de aceite para freír","> pimienta negra molida","> Sal a gusto"], steps: ["1. Poner en un cuenco la harina de trigo. Añade 1 cucharada pequeña de levadura química (tipo Royal no de panadería), una cucharada pequeña de sal y mezclar bien.", "2. Cortar una cebolla grande en rodajas gruesas. Separa los aros de la cebolla. Luego echar al cuenco con la harina y mezclar para que la cebolla se cubra bien de harina. Sacar los aros a un plato y reservar (No tirar la harina que nos servirá para hacer el rebozado de los aros de cebolla).", "3. Batir un huevo grande en un cuenco y luego añade 1/4 de litro de leche. Echar también una pizca de pimienta negra molida y batir para que se mezcle todo.","4. Echar los huevos con la leche en el cuenco con la harina y batir hasta que esté todo bien mezclado. Preparar un recipiente con pan rallado.","5. Poner a fuego medio fuerte una sartén con medio litro de aceite de girasol.","6. Sumergir algunos aros en la mezcla de huevos, leche y harina. Luego sacarlos con un tenedor y deja que escurran.","7. Pasar los aros al recipiente con el pan rallado y rebozarlos bien.","8. Sumergir los aros de cebolla en el aceite caliente hasta que se estén dorados por ambos lados. Luego sacarlos de la sartén y déjalos sobre un plato con papel absorbente para que suelten el exceso de aceite.","9. Repite con los demás aros hasta que estén todos hechos. Por último solo nos queda añadir una pizca de sal."], image: #imageLiteral(resourceName: "ENTRADA Aros cebolla"), info: "Las rabas con estilo")
        platos.append(plate)
        
        plate = Recipe(name: "BRUSCHETTAS CON ACEITUNAS", ingredients: ["> 1 baguette mediana","> Aceite de oliva","> 200 grs de queso de cabra","> 1/4 de taza de aceitunas verdes picadas","> 1/4 de taza de aceitunas negras picadas","> 1/4 de taza de tomate picado","> 1 cucharadita de orégano","> Pimienta negra molida","> Sal a gusto"], steps: ["1. Cortar la baguette en rebanadas finas, barnizar con un poco de aceite de oliva y hornear a 170º hasta que las piezas estén tostadas.", "2. Sobre los trozos de baguette untar el queso de cabra y reservar.", "3. Mezclar las aceitunas con el tomate, un poco de aceite de oliva, el orégano, sal y pimienta.","Colocar un poco de esta mezcla sobre el queso de cabra, y servirse."], image: #imageLiteral(resourceName: "ENTRADA Bruschettas"), info: "Un tentempié diferente")
        platos.append(plate)
        
            // DULCES
        
        plate = Recipe(name: "BROWNIES", ingredients: ["> 100gr de manteca","> 150 gs de chocolate semi amargo","> 2 huevos","> 1 taza de azúcar","> 100gr de harina 0000"], steps: ["1. Derretir el chocolate junto con la manteca a baño maría (o microondas).", "2. Batir el azúcar con los huevos hasta que quede una mezcla blanca.", "3. Agregarle el chocolate derretido y batir hasta que quede una mezcla homogénea.","4. En este punto agregas las nueces o lo que le quieras agregar a tu brownie, o simplemente lo dejas sin nada como el mío.","5. Agregar la harina tamizada en 2 veces e integrar.","6. Colocar en una placa enmantecada y encacaoada (en vez de harina le pones cacao y queda un mil veces mejor) horno fuerte (220 aprox) por 20 minutos o menos, tantear los bordes y la crosta de arriba.","7. Te recomiendo que antes de mandar al horno la mezcla, le pegues una batida bien potente. El horno precalentado y a temperatura alta también ayuda a este efecto."], image: #imageLiteral(resourceName: "POSTRE Brownie"), info: "Con un buen café")
        platos.append(plate)
        
        plate = Recipe(name: "FLAN DE LECHE CONDENSADA", ingredients: ["> 1 lata de leche condensada","> 4 huevos","> 500 cc de leche","> 1 cucharada de esencia de vainilla","> Azúcar cantidad necesaria para caramelo"], steps: ["1. Poner el azúcar en el molde y calentar a fuego directo hasta formar un caramelo rubio, reservar.", "2. En una licuadora mezclar todos los ingredientes.", "3. Volcar la preparación en el molde y llevar a horno mínimo a baño maria por 1 hora aproximadamente.","4. Una vez a temperatura ambiente desmoldar y servir (con mucha crema y mucho dulce de leche)."], image: #imageLiteral(resourceName: "POSTRE Flan"), info: "Sale mixto")
        platos.append(plate)
        
        plate = Recipe(name: "BUÑUELOS", ingredients: ["> 3 huevos","> 8 cucharadas soperas de azucar","> 500grs de harina leudante","> 1 cdta de polvo de hornear","> Leche (c/n para unir)","> Rayadura de 1 limón","> Aciete de girasol (para freir)"], steps: ["1. Batir los huevos con el azucar hasta espumar.", "2. Una vez lograda la espuma agregar la rayadura de limón, la harina tamizada y la leche de a poco. Mezclar bien hasta que la masa quede chiclosa (no muy liquida).", "3. En una olla colocar abundante aceite y calentar. Una vez que el aceite este bien caliente con una cuchara ir colocando 'bollitos' con cuidado de que no se toquen entre ellos.","4. Freir hasta dorar bien.","Sacar los buñuelos del aceite y pasarlos por un bowl con azucar blanca. Otra alternativa espolvorearlos con azucar impalpable."], image: #imageLiteral(resourceName: "POSTRE Buñuelos"), info: "Ideales para el mate")
        platos.append(plate)
        
        plate = Recipe(name: "ALFAJORES SABLÉ", ingredients: ["> 200 grs de manteca fría","> 250 grs de harina 0000","> 80grs de azúcar impalpable","> 1 huevo","> 1 cucharada de esencia de vainilla","> 1 cucharadita de sal rosa o común (entrefina)","> Dulce de leche (o nutella) a gusto"], steps: ["1. Tamizar él harina con el azúcar y la sal. Hacer un hueco en el medio e incorporar la manteca en cubos fría, la esencia de vainilla y el huevo.", "2. Formar un arenado con la punta de los dedos o un cornel (para no darle mucha temperatura a la manteca). Bajar a la mesada y unir la masa sin amasar.", "3. Llevar a la heladera o freezer hasta que la masa esté firme y se pueda trabajar con palote.","4. Estirar y con un cortante de la forma que más les guste cortar las tapitas. Hornear a 180 grados por 8 a 10 min o hasta que estén levemente doradas. Cuanto más doradas más crocantes. Enfriar y rellenar con dulce de leche, nutella, ¡o lo que más te guste!"], image: #imageLiteral(resourceName: "POSTRE Alfajorcitos"), info: "Con mucho dulce de leche")
        platos.append(plate)
        
        plate = Recipe(name: "CHEESECAKE (SIN HORNO)", ingredients: ["> 200grs de galletitas Lincoln","> 100grs de manteca derretida","> Leche condensada 1 pote","> 400grs de queso crema","> 250cc de crema de leche","> 100grs de azúcar","> Jugo de 1/2 limón","> 10grs de gelatina sin sabor","> Frutos rojos frescos o congelados a gusto"], steps: ["1. Procesar las galletitas y mezclarlas con la manteca derretida. Estirar en un molde, presionar.", "2. En un bowl, batir la leche condensada con el queso crema.", "3. Agregar él jugó de limon.","4. Por otro lado hidratar y disolver la gelatina.","5. En otro bowl batir la crema a medio punto con el azúcar.","6. Incorporar la gelatina disuelta a la crema de queso y agregar por último la crema a medio punto.","7. Mezclar bien y volcar sobre la base.","8. Llevar a la heladera por un par de horas (o al freezer si sos ansioso como yo 1 hora).","9. En una sartén poner los frutos rojos con azúcar y dejar que espese, enfriar, volcar sobre la torta ya firme. ¡Listo!"], image: #imageLiteral(resourceName: "POSTRE Cheesecake"), info: "Simplemente irresistible")
        platos.append(plate)
        
        
       
    }

    // MARK: - UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 5
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        
        return 1
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

       let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell", for: indexPath) as! RecipeCell
        
        // ENSALADAS
        if comida == menu[0]{
        switch indexPath.section {
        case 0:
            let meal = platos[0]
            let cellID = "RecipeCell"

            let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                cell.miniImage.image = meal.image
                cell.nameLabel.text = meal.name
                cell.timeLabel.text = meal.info
                cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                // imagenes redondeadas
                cell.miniImage.layer.cornerRadius = 5.0
                cell.miniImage.clipsToBounds = true
                
                return cell
            
            
        case 1:
                let meal = platos[1]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
            
        case 2:
                let meal = platos[2]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
            
        case 3:
                let meal = platos[3]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
            
        case 4:
                let meal = platos[4]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
        default: break
            }
        }
        
        // PLATOS
        if comida == menu[1]{
        switch indexPath.section {
        case 0:
            let meal = platos[5]        // agarra la fila seleccionada
            let cellID = "RecipeCell"       // agarra la celda en general

            let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                cell.miniImage.image = meal.image
                cell.nameLabel.text = meal.name
                cell.timeLabel.text = meal.info
                cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                // imagenes redondeadas
                cell.miniImage.layer.cornerRadius = 5.0
                cell.miniImage.clipsToBounds = true
                
                return cell
            
            
        case 1:
                let meal = platos[6]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
            
        case 2:
                let meal = platos[7]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
           
        case 3:
                let meal = platos[8]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
            
        case 4:
                let meal = platos[9]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
        default: break
            }
        }
        
        // VEGETARIANOS
        if comida == menu[2]{
        switch indexPath.section {
        case 0:
            let meal = platos[10]        // agarra la fila seleccionada
            let cellID = "RecipeCell"       // agarra la celda en general

            let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                cell.miniImage.image = meal.image
                cell.nameLabel.text = meal.name
                cell.timeLabel.text = meal.info
                cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                // imagenes redondeadas
                cell.miniImage.layer.cornerRadius = 5.0
                cell.miniImage.clipsToBounds = true
                
                return cell
            
            
        case 1:
                let meal = platos[11]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
            
        case 2:
                let meal = platos[12]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
            
        case 3:
                let meal = platos[13]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
            
        case 4:
                let meal = platos[14]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
        default: break
            }
        }
        
        // GUARNICIONESˇ
        if comida == menu[3]{
        switch indexPath.section {
        case 0:
            let meal = platos[15]        // agarra la fila seleccionada
            let cellID = "RecipeCell"       // agarra la celda en general

            let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                cell.miniImage.image = meal.image
                cell.nameLabel.text = meal.name
                cell.timeLabel.text = meal.info
                cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                // imagenes redondeadas
                cell.miniImage.layer.cornerRadius = 5.0
                cell.miniImage.clipsToBounds = true
                
                return cell
            
            
        case 1:
                let meal = platos[16]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
            
        case 2:
                let meal = platos[17]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
            
        case 3:
                let meal = platos[18]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
            
        case 4:
                let meal = platos[19]
                let cellID = "RecipeCell"

                let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                    cell.miniImage.image = meal.image
                    cell.nameLabel.text = meal.name
                    cell.timeLabel.text = meal.info
                    cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                
                    // imagenes redondeadas
                    cell.miniImage.layer.cornerRadius = 5.0
                    cell.miniImage.clipsToBounds = true
                    
                    return cell
            
        default: break
            }
        }
        
        // POSTRES
        if comida == menu[4]{
        switch indexPath.section {
        case 0:
            let meal = platos[20]
            let cellID = "RecipeCell"

            let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                cell.miniImage.image = meal.image
                cell.nameLabel.text = meal.name
                cell.timeLabel.text = meal.info
                cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
            
                // imagenes redondeadas
                cell.miniImage.layer.cornerRadius = 5.0
                cell.miniImage.clipsToBounds = true
                
                return cell
            
        case 1:
                   let meal = platos[21]
                   let cellID = "RecipeCell"

                   let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                       cell.miniImage.image = meal.image
                       cell.nameLabel.text = meal.name
                       cell.timeLabel.text = meal.info
                       cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                   
                       // imagenes redondeadas
                       cell.miniImage.layer.cornerRadius = 5.0
                       cell.miniImage.clipsToBounds = true
                       
                       return cell
            
        case 2:
                   let meal = platos[22]
                   let cellID = "RecipeCell"

                   let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                       cell.miniImage.image = meal.image
                       cell.nameLabel.text = meal.name
                       cell.timeLabel.text = meal.info
                       cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                   
                       // imagenes redondeadas
                       cell.miniImage.layer.cornerRadius = 5.0
                       cell.miniImage.clipsToBounds = true
                       
                       return cell
            
        case 3:
                   let meal = platos[23]
                   let cellID = "RecipeCell"

                   let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                       cell.miniImage.image = meal.image
                       cell.nameLabel.text = meal.name
                       cell.timeLabel.text = meal.info
                       cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                   
                       // imagenes redondeadas
                       cell.miniImage.layer.cornerRadius = 5.0
                       cell.miniImage.clipsToBounds = true
                       
                       return cell
            
        case 4:
                   let meal = platos[24]
                   let cellID = "RecipeCell"

                   let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
                       cell.miniImage.image = meal.image
                       cell.nameLabel.text = meal.name
                       cell.timeLabel.text = meal.info
                       cell.ingredientsLabel?.text = "\(meal.ingredients.count) ingredientes"
                   
                       // imagenes redondeadas
                       cell.miniImage.layer.cornerRadius = 5.0
                       cell.miniImage.clipsToBounds = true
                       
                       return cell
            
        default: break
            }
        }
        
        return cell
    }
            
    
    // funcion para compartir, eliminar, etc

        
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
            
            // Compartir
            
        let shareAction = UIContextualAction(style: .normal, title: "Compartir") { (action, sourceView, completionHandler) in
            
            
            let shareDefaultText = "Mirá ésta receta hecha por @BelBenzo 🙌🏼"
            
            let shareImage = self.platos[indexPath.section].image
            
            let activityController = UIActivityViewController(activityItems: [shareDefaultText, shareImage], applicationActivities: nil)
            
                
            self.present(activityController, animated: true, completion: nil)
            }
        
            // color de fondo del boton COMPARTIR
            shareAction.backgroundColor = UIColor(displayP3Red: 0.2, green: 0.2, blue: 0.9, alpha: 1.0)
            
    /*       // Eliminar
            
            let deleteAction = UIContextualAction(style: .destructive, title: "Borrar") { (action, sourceView, completionHandler) in
                self.platos.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            // color de fondo del boton BORRAR
            deleteAction.backgroundColor = UIColor(displayP3Red: 0.9, green: 0.1, blue: 0.1, alpha: 1.0)
            
    */
            // opciones de deslizado
            let swypeConfiguration = UISwipeActionsConfiguration(actions: [shareAction])
            
            return swypeConfiguration
 
            
    // Despues de configurar una celda, agregarla al UIController desde la parte grafica
    // al delegate y datos (2) arrastrandola.

    }
 
    
    
   
    
    //MARK: - UITableViewDelegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
 

    // SEGUE
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails"{
            
            if let ruta = self.tableView.indexPathForSelectedRow{
                
            // ENSALADAS
            if comida == menu[0]{
            switch ruta.section {
            case 0:
                    let selectedRecipe = self.platos[0]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                //  print("He seleccionado la seccion \(ruta.section)")

            case 1:
                    let selectedRecipe = self.platos[1]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 2:
                    let selectedRecipe = self.platos[2]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 3:
                    let selectedRecipe = self.platos[3]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 4:
                    let selectedRecipe = self.platos[4]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
            
            default: break
            }
            }
                
            // PLATOS
            if comida == menu[1]{
            switch ruta.section {
            case 0:
                    let selectedRecipe = self.platos[5]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                //  print("He seleccionado la seccion \(ruta.section)")

            case 1:
                    let selectedRecipe = self.platos[6]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 2:
                    let selectedRecipe = self.platos[7]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 3:
                    let selectedRecipe = self.platos[8]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 4:
                    let selectedRecipe = self.platos[9]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
            
            default: break
            }
            }
            
            // VEGETARIANA
            if comida == menu[2]{
            switch ruta.section {
            case 0:
                    let selectedRecipe = self.platos[10]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                //  print("He seleccionado la seccion \(ruta.section)")

            case 1:
                    let selectedRecipe = self.platos[11]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 2:
                    let selectedRecipe = self.platos[12]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 3:
                    let selectedRecipe = self.platos[13]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 4:
                    let selectedRecipe = self.platos[14]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
    
            default: break
            }
            }
                
            // GUARNICIONES
            if comida == menu[3]{
            switch ruta.section {
            case 0:
                    let selectedRecipe = self.platos[15]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                //  print("He seleccionado la seccion \(ruta.section)")

            case 1:
                    let selectedRecipe = self.platos[16]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 2:
                    let selectedRecipe = self.platos[17]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 3:
                    let selectedRecipe = self.platos[18]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 4:
                    let selectedRecipe = self.platos[19]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
            
            default: break
            }
            }
            
            // POSTRES
            if comida == menu[4]{
            switch ruta.section {
            case 0:
                    let selectedRecipe = self.platos[20]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
            case 1:
                    let selectedRecipe = self.platos[21]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                        
            case 2:
                    let selectedRecipe = self.platos[22]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 3:
                    let selectedRecipe = self.platos[23]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                
            case 4:
                    let selectedRecipe = self.platos[24]
                    let destinationViewController = segue.destination as! DetailViewController
                    destinationViewController.platillo = selectedRecipe
                    
            default: break
                }
                }
}
}
}
    
}



