
//
//  Created by Pablo  on 15/12/2019.
//  Copyright © 2019 Pablo Benzo. All rights reserved.
//

import UIKit
import MessageUI
import Photos

class ViewController: UITableViewController {
// contiene (UIViewController, UITableViewDataSource, UITableViewDelegate)
    
    var recetas : [Recipe] = []
    let telefono = "1130745924"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
                
        /*
        // Barra de busqueda
        self.barraBusqueda = UISearchController(searchResultsController: nil)
        self.tableView.tableHeaderView = self.barraBusqueda.searchBar
        */
        
        // Pedir permisos fotos
        
        
        
        /* Presentar pantalla previa (Navigation Controller)
        if let vc = storyboard?.instantiateViewController(identifier: "presentacion"){
            navigationController?.present(vc, animated: true, completion: nil)
        }
        */
        
        tableView.separatorColor = UIColor(displayP3Red: 0.9, green: 0.9, blue: 0.9, alpha: 0.0)
        // Alpha separador recetas
        
        
        // Configurar texto del boton BACK
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Platos", style: .plain, target: nil, action: nil)
        
                
        var section = Recipe(name: "ENSALADAS", ingredients: [""], steps: [""], image: #imageLiteral(resourceName: "ensalada"), info: "De todos colores y sabores")
        recetas.append(section)
        
        section = Recipe(name: "CLÁSICAS", ingredients: [""], steps: [""], image: #imageLiteral(resourceName: "clasica"), info: "Recetas clasicas para todos")
        recetas.append(section)
        
        section = Recipe(name: "VEGETARIANAS", ingredients: [""], steps: [""], image: #imageLiteral(resourceName: "vegetariana"), info: "Para amantes de lo verde")
        recetas.append(section)
        
        section = Recipe(name: "GUARNICIONES", ingredients: [""], steps: [""], image: #imageLiteral(resourceName: "guarnicion"), info: "Para acompañamiento")
        recetas.append(section)
        
        section = Recipe(name: "DULCES", ingredients: [""], steps: [""], image: #imageLiteral(resourceName: "dulce"), info: "El permitido de la semana")
        recetas.append(section)
        
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Consultar si vio el tutorial, si lo vio NO MOSTRAR, si no seguir
        
        let porDefecto = UserDefaults.standard
        let haVistoElTutorial = porDefecto.bool(forKey: "haVistoElTutorial")
        
        if haVistoElTutorial {
            return
        }
        
        // Presentar VC del tutorial (TutorialPageViewController)
        if let pageVC = storyboard?.instantiateViewController(identifier: "TutorialPageVC") as? TutorialPageViewController {
            
            self.present(pageVC, animated: true, completion: nil)
        }
        
        
       // dismiss(animated: true, completion: nil)
    }
    
    
    // Ocultar y aparecer barra de navegacion al SWIPEAR
    
    func willBeAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = true
    }
    
    // Boton TUTORIAL
    @IBAction func infoTutorial(_ sender: UIBarButtonItem) {
        
        if let pageVC = storyboard?.instantiateViewController(identifier: "TutorialPageVC") as? TutorialPageViewController {
            
            self.present(pageVC, animated: true, completion: nil)
        }
    }
    
    // Boton COMPARTIR
    @IBAction func graciasBoton(_ sender: UIBarButtonItem) {
        
        /*
        let textoCompartir = "Mira, te comparto la app de recetas de BelBenzo: https://apps.apple.com/ar/app/le-menu/id1499570862"
        
        let activityController = UIActivityViewController(activityItems: [textoCompartir], applicationActivities: nil)
            
        self.present(activityController, animated: true, completion: nil)
        */
        
        let alertControlador = UIAlertController(title: "Enviar SMS", message: "¿Querés contactarte con el creador de la app? mandale un mensaje.", preferredStyle: .alert)
        
        let smsAccion = UIAlertAction(title: "Mensaje", style: .default) { (action) in
            
            if MFMessageComposeViewController.canSendText() {       // Si se puede enviar sms (app configurada)
                let msg = "¡Hola! Quiero contactarme para hablar sobre el desarrollo de una app."
                let msgVC = MFMessageComposeViewController()
                msgVC.body = msg
                msgVC.recipients = [self.telefono]
                msgVC.messageComposeDelegate = self
                
                self.present(msgVC, animated: true, completion: nil)
                
            }
        }
        
        alertControlador.addAction(smsAccion)
        
        let smsCancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alertControlador.addAction(smsCancel)
        
        
        self.present(alertControlador, animated: true, completion: nil)
        
    }
    
    
    
    
    /* 2- Realizar busqueda con filtrado
    func filtrarContenidoPara(textoBuscar: String){
        
        self.resultadoBusqueda = self.recetas.filter({ (Recipe) -> Bool in
            let nombreBuscar = Recipe.name.range(of: textoBuscar, options: NSString.CompareOptions.caseInsensitive)
            return nombreBuscar != nil
        })
    } */
    
    
    //MARK: - UITableViewDataSource
    
    // Funcion de CELDA: para configurar informacion
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recetas.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let meal = recetas[indexPath.row]
        
        // celda individual
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell", for: indexPath) as! RecipeCell
        cell.miniImage.image = meal.image
        cell.nameLabel.text = meal.name
        
        
        
        
        // imagenes redondeadas
        cell.miniImage.layer.cornerRadius = 10.0
        cell.miniImage.clipsToBounds = true
        
 
        return cell
                
    }
    
    /* funcion para borrar deslizando (simple)

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete{
            self.recetas.remove(at: indexPath.row)
        }
        
        self.tableView.deleteRows(at: [indexPath], with: .fade)
    }
    
    */
  
    
  
    
    //MARK: - UITableViewDelegate
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    // SEGUE
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showPlates"{
            if let filaElegida = self.tableView.indexPathForSelectedRow {
                let recetaElegida = self.recetas[filaElegida.row]
                
                let destinationViewController = segue.destination as! PlateViewController
                destinationViewController.comida = recetaElegida
                destinationViewController.menu = self.recetas
                
            }
            
        }
    }
    

    

}

/* Protocolo que actualiza la vista
extension ViewController : UISearchResultsUpdating{
    
    
}*/

// El delegado avisa que el usuario terminó de escribir el SMS

extension ViewController : MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult){
        
        controller.dismiss(animated: true, completion: nil)     // Cerrar pagina sms
        print(result)
    }
}
