//
//  FotosSubidasViewController.swift
//  07- MisRecetas
//
//  Created by Pablo  on 05/03/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit
import Photos

    let reuseIdentifier = "Celda"

    var recuerdos : [URL] = []

    class FotosSubidasViewController: UICollectionViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pedirPermisosFotos()
        self.cargarRecuerdos()

        // Crear boton + en la nav bar
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self,     action: #selector(self.agregarImagenSeleccionada))

        // Register cell classes
        //self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

    }
    
    

   // 1.1. Verificar permisos

    func pedirPermisosFotos() {
        PHPhotoLibrary.requestAuthorization { (estadoAutoriz) in

            DispatchQueue.main.async {            // Mover todo el codigo a tiempo real (principal)
                if estadoAutoriz != .authorized{

                    let alertaController = UIAlertController(title: "¡Atención!", message: "Por favor, autorizar acceso a fotos en la configuración de la app, para poder cargar fotos de tu recetas en nuestra galeria de fotos." , preferredStyle: .actionSheet)

                    let alertaAction = UIAlertAction(title: "OK", style: .default) { (listo) in
                        self.navigationController?.popViewController(animated: true)
                    }

                    alertaController.addAction(alertaAction)

                    self.present(alertaController, animated: true)
                    
                }
                else {
                    
                    if recuerdos.isEmpty {
                    
                    let tusRecetas = UIAlertController(title: "¡Mostranos el final!", message: "Agregá tus platos hechos y compartilos tocando la foto 🌅 (agregá más platos mas tarde con el signo '+' en la esquina superior).", preferredStyle: .alert)

                    let tusReceAccion = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.agregarImagenSeleccionada()
                        }

                    tusRecetas.addAction(tusReceAccion)

                    self.present(tusRecetas, animated: true, completion: nil)
                        
                    
                    }
                }
            }
        }
    }
    
///   // UIImagePickerControllerDelegate

    // 1.3. Cargar imagenes

    func cargarRecuerdos(){
        recuerdos.removeAll()  // Vaciamos la variable

        guard let archivos = try? FileManager.default.contentsOfDirectory(at: obtenerDirectorioDocs(), includingPropertiesForKeys: nil, options: [])    // Guardamos en ARCHIVOS el array en 👆🏼
            else {
                return
            }

        // 1.4. Rellena el array RECUERDOS con imagenes sin .thumb
        for archivo in archivos {

            let nombreArchivo = archivo.lastPathComponent

                if nombreArchivo.hasSuffix(".thumb") {
                    let sinExtension = nombreArchivo.replacingOccurrences(of: ".thumb", with: "")

                    let rutaMemoria = obtenerDirectorioDocs().appendingPathComponent(sinExtension)
                    recuerdos.append(rutaMemoria)

                }
            }

        // Refresca la collection view
        self.collectionView.reloadData()

        }

    // 1.2. Crear directorio para guardar las fotos

    func obtenerDirectorioDocs() -> URL {
        let ruta = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let directorioDocumentos = ruta[0]
        return directorioDocumentos
    }

    
    
///   // UICollectionViewController
    

    // 2. Mostrar carrete para seleccionar fotos por el usuario (signo +)

    @objc func agregarImagenSeleccionada () {

        let vc = UIImagePickerController()
        vc.modalPresentationStyle = .formSheet
        vc.delegate = self
        navigationController?.present(vc, animated: true, completion: nil)
    }

    // 3. Agregar imagen seleccionada a variable (OK y Cancelar, éste es el OK)

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let laImagen = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.agregarRecuerdo(image: laImagen)
            self.cargarRecuerdos()

            dismiss(animated: true)     // Ocultar fototeca al elegir foto
        }
    }

    // 4. Tomamos la imagen seleccionada por el usuario

    func agregarRecuerdo(image: UIImage){
        let nombreRecuerdo = "memory-\(Date().timeIntervalSince1970)"

        let nombreImagen = "\(nombreRecuerdo).jpg"
        let nombreThumb = "\(nombreRecuerdo).thumb"

        // 5. Comprimimos y guardamos la IMAGEN en archivo, en la app

        do {
            let rutaImagen = obtenerDirectorioDocs().appendingPathComponent(nombreImagen)

            if let jpegData = image.jpegData(compressionQuality: 80) {
                try! jpegData.write(to: rutaImagen, options: .atomicWrite)
            }

            // 7. Comprimimos y guardamos la THUMB en archivo, en la app

            if let thumbail = achicarImagen(image: image, to: 200) {
                let rutaThumb = obtenerDirectorioDocs().appendingPathComponent(nombreThumb)

                if let jpegData = thumbail.jpegData(compressionQuality: 80) {
                    try! jpegData.write(to: rutaThumb, options: .atomicWrite)
                }
        }
    }

}

    // 6. Achicamos la imagen para hacer Thumbs

       func achicarImagen(image: UIImage, to width: CGFloat) -> UIImage? {
           let factorEscala = width / image.size.width

           let height = image.size.height * factorEscala

           UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), false, 0)
           image.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
           let nuevaImagen = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()

           return nuevaImagen
    }
 

    // 8. Agregarle las extensiones a las imagenes a mostrar

    func imagenURL(for memory: URL) -> URL {
        return memory.appendingPathExtension("jpg")
    }

    func thumbURL(for memory: URL) -> URL {
        return memory.appendingPathExtension("thumb")
    }



// SEPARADOR - SEPARADOR - SEPARADOR


    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        } else {
            return recuerdos.count
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Celda", for: indexPath) as! FotosSubidasViewCell

        let memoria = recuerdos[indexPath.row]
        let nombreMemoria = thumbURL(for: memoria).path
        let imagen = UIImage(contentsOfFile: nombreMemoria)
        cell.celdaImagen.image = imagen

        // Caracteristicas de cada celda
        cell.layer.borderColor = UIColor.white.cgColor
        cell.layer.borderWidth = 4
        cell.layer.cornerRadius = 10
        
        // Gesto de pulsacion larga
        if cell.gestureRecognizers == nil {
            
            let pulsacion = UILongPressGestureRecognizer(target: self, action: #selector(self.memoriaPresionLarga))
            pulsacion.minimumPressDuration = 0.1
            cell.addGestureRecognizer(pulsacion)
            
        }

        return cell
    }
        
        
        @objc func memoriaPresionLarga(sender: UILongPressGestureRecognizer){

                let shareDefaultText = "Te comparto ésta delicia hecha con el recetario de Belén 🤤"
                    
            let memoria = recuerdos[0]
            let nombreMemoria = thumbURL(for: memoria).path
            let imagen = UIImage(contentsOfFile: nombreMemoria)
                let shareImage = imagen
                       
            let activityController = UIActivityViewController(activityItems: [shareDefaultText, shareImage as Any], applicationActivities: nil)
                       
                self.present(activityController, animated: true, completion: nil)

        }
//
//    // MARK: UICollectionViewDelegate
//
//    /*
//    // Uncomment this method to specify if the specified item should be highlighted during tracking
//    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//    */
//
//    /*
//    // Uncomment this method to specify if the specified item should be selected
//    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//    */
//
//    /*
//    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
//    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
//        return false
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
//        return false
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
//
//    }
//    */

}


