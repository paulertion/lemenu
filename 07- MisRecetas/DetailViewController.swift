//
//  DetailViewController.swift
//  07- MisRecetas
//
//  Created by Pablo  on 03/01/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet var imageDetail: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var ratingButton: UIButton!
    
    var platillo : Recipe!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = platillo.name   //titulo de la receta seleccionada
        
        tableView.backgroundColor = UIColor(displayP3Red: 0.9, green: 0.9, blue: 0.9, alpha: 0.25)  // color de fondo de la plantilla
        tableView.tableFooterView = UIView(frame: CGRect.zero)  // ocultar filas vacias despues de la ultima
        tableView.separatorColor = UIColor(displayP3Red: 0.9, green: 0.9, blue: 0.9, alpha: 0.45)   // config color de separadores

        self.imageDetail.image = self.platillo.image
        
        // cargar imagen de RATING antes de arrancar la app
        let imagen = UIImage(named: self.platillo.rating)
        self.ratingButton.setImage(imagen, for: .normal)
        
        
       
// ajustar filas automaticamente a la fuente (esta hecho por VISTA)
        /*    self.tableView.estimatedRowHeight = 50.0
        self.tableView.rowHeight = UITableView.automaticDimension   */
        
    }
    

    
    // ocultar y aparecer barra de navegacion al SWIPEAR
    func willBeAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
 
}

// funcion de CELDA: para dirigir FILA(receta) a su descripcion

extension DetailViewController: UITableViewDataSource {

func numberOfSections(in tableView: UITableView) -> Int {
    2
}

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    switch section {
    case 0:
        return self.platillo.ingredients.count
    case 1:
        return self.platillo.steps.count
    default:
        return 0
    }
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeDetailCell", for: indexPath) as! RecipeDetailViewCell //llama a la clase (DATOS)
    
    cell.backgroundColor = UIColor.clear
    
    switch indexPath.section {
    case 0:
        cell.valueLabel.text = self.platillo.ingredients[indexPath.row]
    case 1:
        cell.valueLabel.text = self.platillo.steps[indexPath.row]
    default:
        break
    }
    
    return cell
    }
    
    // Agregar titulo a cada seccion
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title = ""
        
        switch section {
        case 0:
            title = "Ingredientes"
        case 1:
            title = "Procedimiento"
        default:
            break
        }
        
        return title
}
    

    

// Despues de configurar una celda, agregarla al UIController desde la parte grafica
// al delegate y datos (2) arrastrandola.

    
   // SEGUE
   
//   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//       if segue.identifier == "showPeople"{
//
//
//       }
//   }

    
}

extension DetailViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)

    
    }
    
}
