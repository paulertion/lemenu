//
//  SingleViewController.swift
//  07- MisRecetas
//
//  Created by Pablo  on 20/12/2019.
//  Copyright © 2019 Pablo Benzo. All rights reserved.
//

import UIKit

class SingleViewController: UIViewController {
    
    var dinner : [Recipe] = []
    
    @IBOutlet var tableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
 /*       self.tableView.dataSource = self
        self.tableView.delegate = self          */
        
        
        var plate = Recipe(name: "Brownies", time: 12, ingredients: ["chocolate","leche","nueces"], steps: ["mezclar ingredientes", "poner en el horno", "darle forma"], image: #imageLiteral(resourceName: "Brownies"), info: "")
            dinner.append(plate)
            
            plate = Recipe(name: "Cookies rellenas", time: 10, ingredients: ["harina","chips de chocolate","azucar impalpable","manteca"], steps: ["mezclar harina con manteca", "poner en el horno", "darle forma","agregar chips de chocolate"], image: #imageLiteral(resourceName: "galletas"), info: "")
            dinner.append(plate)
            
            plate = Recipe(name: "Buñuelos dulces", time: 30, ingredients: ["harina","leche","azucar"], steps: ["mezclar ingredientes","darle forma","poner en el horno"], image: #imageLiteral(resourceName: "Buñuelos"), info: "")
            dinner.append(plate)
            
            plate = Recipe(name: "Carrotcake", time: 40, ingredients: ["zanahoria","leche","huevos","chocolate rallado"], steps: ["mezclar ingredientes", "poner en el horno", "darle forma","agregar chocolate rayado"], image: #imageLiteral(resourceName: "Carrot"), info: "")
            dinner.append(plate)
            
            plate = Recipe(name: "Cheesecake de frutos rojos", time: 45, ingredients: ["queso","leche","frutos rojos","harina","azucar"], steps: ["mezclar ingredientes", "poner en el horno", "darle forma","agregar frutos"], image: #imageLiteral(resourceName: "cheesecake"), info: "")
            dinner.append(plate)

        }

}


extension SingleViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dinner.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let recipe = dinner[indexPath.row] // agarra la fila, no columna(seccion)
        let cellID = "RecipeCell"           // agarra la celda en general
        
        // celda individual
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        cell.textLabel?.text = recipe.name
        cell.imageView?.image = recipe.image
        return cell
    }
    
}
