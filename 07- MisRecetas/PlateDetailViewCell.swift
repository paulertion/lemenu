//
//  PlateDetailViewCell.swift
//  07- MisRecetas
//
//  Created by Pablo  on 16/01/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit

class PlateDetailViewCell: UITableViewCell {

    @IBOutlet var plateImage: UIImageView!
    @IBOutlet var plateTitle: UILabel!
    @IBOutlet var plateTime: UILabel!
    @IBOutlet var plateIngredients: UILabel!
    
    var recetas : Recipe!
    
    func viewDidLoad() {
    
        
        
        
    }
}
