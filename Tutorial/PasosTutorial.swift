//
//  PasosTutorial.swift
//  07- MisRecetas
//
//  Created by Pablo  on 23/02/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import Foundation
import UIKit

class PasosTutorial : NSObject {
    var index = 0
    var titulo : String
    var contenido : String
    var imagen : UIImage!
    
    init(index: Int, titulo: String, contenido: String, imagen: UIImage){
        self.index = index
        self.titulo = titulo
        self.contenido = contenido
        self.imagen = imagen
    }
}
