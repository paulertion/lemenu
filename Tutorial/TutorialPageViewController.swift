//
//  TutorialPageViewController.swift
//  07- MisRecetas
//
//  Created by Pablo  on 23/02/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit

class TutorialPageViewController: UIPageViewController {
    
    var pasosTotal : [PasosTutorial] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        let pasoUno = PasosTutorial(index: 0, titulo: "Benvenidos", contenido: "Gracias por bajarte mi app ♡ Las recetas se irán actualizando semana a semana incorporando nuevas a la lista. Proximamente añadiremos nuevas secciones, ¡estate aten@!", imagen: #imageLiteral(resourceName: "paso01"))
        pasosTotal.append(pasoUno)
        
        let pasoDos = PasosTutorial(index: 1, titulo: "Compartí las que te gustan", contenido: "Deslizando las recetas hacia la izquierda podes compartirlas en Facebook, Twitter, Instagram, por mensaje, o a donde quieras. Podes agregarle modificaciones o comentarios 🍽", imagen: #imageLiteral(resourceName: "paso02"))
        pasosTotal.append(pasoDos)
        
        let pasoTres = PasosTutorial(index: 2, titulo: "Mostranos tus cualidades", contenido: "Subí la foto de tu plato ya hecho y compartí tu esfuerzo culinario con el mundo, tocando el simbolo '+' en la foto de la receta.", imagen: #imageLiteral(resourceName: "paso03"))
        pasosTotal.append(pasoTres)
        
        let pasoCuatro = PasosTutorial(index: 3, titulo: "Seguime en Instagram", contenido: "Alli podras ver los paso a paso, videotutoriales, y nuevas ideas sin terminar además de eventos o historias en el día. ¡Te espero! 👩🏼‍🍳", imagen: #imageLiteral(resourceName: "paso04"))
        pasosTotal.append(pasoCuatro)
        
        dataSource = self
        if let startVC = self.paginaViewController(enIndex: 0) {
            setViewControllers([startVC], direction: .forward, animated: true, completion: nil)
        }
    }
    
    
    // Funcion para ir hacia adelante en el BOTON
    func forward(toIndex: Int){
        if let nextVC = self.paginaViewController(enIndex: toIndex + 1) {
            self.setViewControllers([nextVC], direction: .forward, animated: true, completion: nil)
        }
    }
    
    
    

}

//MARK: - DataSource

extension TutorialPageViewController : UIPageViewControllerDataSource{
    
    // Pagina anterior
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! TutorialContentViewController).tutorial.index
        index -= 1
        
        return self.paginaViewController(enIndex: index)
    }
    
    
    // Pagina siguiente
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = (viewController as! TutorialContentViewController).tutorial.index
        index += 1
        
        return self.paginaViewController(enIndex: index)
    }
    
    
    // Nos aseguramos que este dentro de las paginas que existen
    func paginaViewController(enIndex: Int) -> TutorialContentViewController? {
        if enIndex == NSNotFound || enIndex < 0 || enIndex >= self.pasosTotal.count {
            return nil
        }
        
        if let paginaContenidoViewController = storyboard?.instantiateViewController(identifier: "TutorialContentVC") as? TutorialContentViewController {
            
            paginaContenidoViewController.tutorial = self.pasosTotal[enIndex]
            
            return paginaContenidoViewController
        }
        
        // por las dudas si el IF falla
        return nil
    }
    
    // Por ultimo agregamos los RETURN del ANTES y DESPUES con la ultima funcion (paginaViewController)
    
    
    /*
    
    // Mostrar 'bolitas' de indicador de pagina (de fabrica)
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.pasosTotal.count
    }
    
    // Mostrar en que 'bolita' estamos posisionados
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        if let contePaginaVC = storyboard?.instantiateViewController(identifier: "TutorialContentVC") as? TutorialContentViewController {
            if let tutorial = contePaginaVC.tutorial{
                return tutorial.index
            }
        }
        return 0
    }
    
    */
    
}
