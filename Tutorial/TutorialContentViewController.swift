//
//  TutorialContentViewController.swift
//  07- MisRecetas
//
//  Created by Pablo  on 23/02/2020.
//  Copyright © 2020 Pablo Benzo. All rights reserved.
//

import UIKit

class TutorialContentViewController: UIViewController {

    @IBOutlet var tituloTuto: UILabel!
    @IBOutlet var imagenTuto: UIImageView!
    @IBOutlet var descripTuto: UILabel!
    @IBOutlet var indicadorTuto: UIPageControl!
    @IBOutlet var botonTuto: UIButton!
    
    
    var tutorial : PasosTutorial!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tituloTuto.text = self.tutorial.titulo
        self.imagenTuto.image = self.tutorial.imagen
        self.descripTuto.text = self.tutorial.contenido
        self.indicadorTuto.currentPage = self.tutorial.index
        
        switch self.tutorial.index {
        case 0...2:
            self.botonTuto.setTitle("Siguiente", for: .normal)
        case 3:
            self.botonTuto.setTitle("¡Entendido!", for: .normal)
        default:
            break
        }

    }
    
    // Accion del boton, ir hacia adelante al presionarlo gracias a la funcion FORWARD(TOINDEX: )
    @IBAction func botonSig(_ sender: UIButton) {
        
        switch self.tutorial.index {
        case 0...2:
            let pageViewController = parent as! TutorialPageViewController
            pageViewController.forward(toIndex: self.tutorial.index)
        case 3:
            let porDefecto = UserDefaults.standard                  // Ocultar para siempre el tutorial
            porDefecto.set(true, forKey: "haVistoElTutorial")
            
            self.dismiss(animated: true, completion: nil)
        default:
            break
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
